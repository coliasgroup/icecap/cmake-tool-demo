cmake_minimum_required(VERSION 3.7.2)

set(project_dir "${CMAKE_CURRENT_LIST_DIR}")
file(GLOB project_modules ${project_dir}/projects/*)
list(
    APPEND
        CMAKE_MODULE_PATH
        ${project_dir}/kernel
        ${project_dir}/tools/seL4/cmake-tool/helpers/
        ${project_dir}/tools/seL4/elfloader-tool/
        ${project_modules}
)

include(application_settings)

set(ARM_CPU "cortex-a57")

set(KernelArch arm CACHE STRING "" FORCE)
set(KernelSel4Arch aarch64 CACHE STRING "" FORCE)
set(KernelPlatform qemu-arm-virt CACHE STRING "" FORCE)
set(KernelArmHypervisorSupport ON CACHE BOOL "" FORCE)
set(KernelVerificationBuild OFF CACHE BOOL "" FORCE)
set(KernelDebugBuild ON CACHE BOOL "" FORCE)
set(KernelMaxNumNodes 2 CACHE STRING "" FORCE)
set(LibSel4FunctionAttributes public CACHE STRING "" FORCE)
set(CMakeForegroundComplexCommands ON CACHE BOOL "" FORCE)

find_package(seL4 REQUIRED)
sel4_configure_platform_settings()

ApplyData61ElfLoaderSettings(${KernelARMPlatform} ${KernelSel4Arch})
