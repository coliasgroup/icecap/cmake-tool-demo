root_task_crate_name := demo-root-task

build_dir := build
sticky_dir := $(build_dir)/sticky
disposable_dir := $(build_dir)/disposable
rust_build_dir := $(sticky_dir)/target

cmake_tool_build_dir := $(disposable_dir)/cmake-tool
sel4_cache_dir := $(disposable_dir)/sel4-cache
misc_build_dir := $(disposable_dir)/misc

src := src
icecap_src := icecap
icecap_rust_src := $(icecap_src)/src/rust


.PHONY: none
none:

.PHONY: clean
clean:
	rm -rf $(disposable_dir)

.PHONY: deep-clean
deep-clean: clean
	rm -rf $(sticky_dir)

.PHONY: very-deep-clean
very-deep-clean: deep-clean
	rm -rf $(build_dir)


mk_dirs := \
	$(cmake_tool_build_dir) \
	$(misc_build_dir)

$(mk_dirs):
	mkdir -p $@


.PHONY: configure
configure: $(cmake_tool_build_dir)
	cmake \
		-G Ninja \
		-DCROSS_COMPILER_PREFIX=aarch64-linux-gnu- \
		-DCMAKE_TOOLCHAIN_FILE=kernel/gcc.cmake \
		-DSEL4_CACHE_DIR=$(abspath sel4_cache_dir) \
		-DROOT_TASK_ELF=$(abspath $(rust_build_dir)/aarch64-icecap/release/$(root_task_crate_name).elf) \
		-C settings.cmake \
		-B $(cmake_tool_build_dir) \
		-S .

.PHONY: build
build:
	ninja -C $(cmake_tool_build_dir)

.PHONY: simulate
simulate:
	cd $(cmake_tool_build_dir) && \
		./simulate --extra-qemu-args="-smp 2"


###
### Called from CMake
###


# inputs
libsel4_include_flags := $(addprefix -I,$(subst ;, ,$(LIBSEL4_INCLUDE_PATHS)))
libsel4_link_flags := $(addprefix -L,$(subst ;, ,$(LIBSEL4_LINK_PATHS)))


kernel_config_json := $(misc_build_dir)/kernel-config.json
kernel_config_prefixes := Kernel LibSel4 HardwareDebugAPI

$(kernel_config_json): $(cmake_tool_build_dir)/CMakeCache.txt | $(misc_build_dir)
	sed -n 's,^\([A-Za-z0-9][^:]*\):\([^=]*\)=\(.*\)$$,\1:\2=\3,p' $< \
		| grep -e '$$.^' $(addprefix -e ,$(kernel_config_prefixes)) \
		| python3 generate_config_json.py \
		> $@


icecap_rustflags := $(libsel4_link_flags)

.PHONY: root-task
root-task: $(kernel_config_json)
	cd $(src) && \
		RUST_TARGET_PATH=$(abspath $(icecap_rust_src)/support/targets) \
		CARGO_TARGET_AARCH64_ICECAP_RUSTFLAGS="$(icecap_rustflags)" \
		BINDGEN_EXTRA_CLANG_ARGS="$(libsel4_include_flags)" \
		SEL4_CONFIG=$(abspath $(kernel_config_json)) \
			cargo build \
				-Z unstable-options \
				-Z build-std=core,alloc,compiler_builtins -Z build-std-features=compiler-builtins-mem \
				--release \
				--target-dir $(abspath $(rust_build_dir)) \
				--target aarch64-icecap \
				-p demo-root-task
