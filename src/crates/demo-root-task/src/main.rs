#![no_std]
#![no_main]
#![feature(proc_macro_hygiene)]

extern crate alloc;

use cfg_if_generic::cfg_if_generic;
use icecap_std::prelude::*;
use icecap_std::sel4::{cfg_sel4, config, BootInfo};

#[cfg_sel4(not(KernelStackBits = "0"))]
#[icecap_task_root::main]
fn main(_: BootInfo) -> Fallible<()> {
    debug_println!(
        "KernelRetypeFanOutLimit: {}",
        config::KernelRetypeFanOutLimit
    );
    cfg_if_generic! {
        cfg_sel4 if #[cfg(KernelArmVtimerUpdateVOffset)] {
            debug_println!("KernelArmVtimerUpdateVOffset");
        } else if #[cfg(any(not(KernelArmDisableWFIWFETraps), KernelNumPriorities = "0"))] {
            compile_error!("");
        }
    }
    Ok(())
}
