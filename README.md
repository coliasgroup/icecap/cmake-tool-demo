# Demo of IceCap + `seL4_tools/cmake_tool`

Upstream IceCap development, testing, and CI use a Nix-based build system.
However, care has been taken to ensure that the IceCap source code itself is build-system agnostic.
This repository demonstrates the use of IceCap crates in a project built by `seL4_tools/cmake_tool` rather than Nix.

The purpose of this repository is to share, by showing, the IceCap crates with those who are unfamiliar with the IceCap build system.
This repository does not constitute a recommendation of how to use IceCap with `seL4_tools/cmake_tool`.
There are certainly better ways than what is shown here.

## Build instructions

Building and running this demo requires Make and Docker.

First, clone this respository and its submodules:

```
git clone --recursive https://gitlab.com/coliasgroup/icecap/cmake-tool-demo.git
cd cmake-tool-demo
```

Next, build, run, and enter a Docker container for development:

```
make -C docker run && make -C docker exec
```

Now, inside the container, configure, build, and simulate a simple system:

```
# inside the container
make configure && make build && make simulate
```
